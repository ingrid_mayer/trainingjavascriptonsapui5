sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function (JSONModel, Device) {
	"use strict";

	return {
		createDeviceModel: function () {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},

		/**
		 * Column Configuration Model
		 * @public
		 * @returns {aColumns}, which represents the array of the added Columns
		 */

		createColumnConfigModel: function () {
			return new JSONModel([{
				"id": "index",
				"name": "Index",
				"type": "Integer",
				"index": 1,
				"width": 110,
				"isVisible": false,
				"isGroupable": true,
				"isFilterable": true,
				"isSortable": true
			}, {
				"id": "name",
				"name": "Name",
				"type": "string",
				"index": 2,
				"width": 110,
				"isVisible": true,
				"isGroupable": true,
				"isFilterable": true,
				"isSortable": true
			}, {
				"id": "surname",
				"name": "Surname",
				"type": "string",
				"index": 3,
				"width": 110,
				"isVisible": true,
				"isGroupable": true,
				"isFilterable": true,
				"isSortable": true
			}, {
				"id": "fullname",
				"name": "Fullname",
				"type": "string",
				"index": 4,
				"width": 110,
				"isVisible": true,
				"isGroupable": true,
				"isFilterable": true,
				"isSortable": true
			}, {
				"id": "age",
				"name": "Age",
				"type": "integer",
				"index": 5,
				"width": 110,
				"isVisible": true,
				"isGroupable": true,
				"isFilterable": true,
				"isSortable": true
			}, {
				"id": "email",
				"name": "Email",
				"type": "string",
				"index": 6,
				"width": 110,
				"isVisible": true,
				"isGroupable": true,
				"isFilterable": true,
				"isSortable": true
			},
			{
				"id": "bday",
				"name": "Birth Date",
				"type": "date",
				"index": 7,
				"width": 110,
				"isVisible": true,
				"isGroupable": true,
				"isFilterable": true,
				"isSortable": true
			}]);
		}
	};
});