sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"../model/models",
	"sap/ui/model/Filter",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/FilterOperator",
	"sap/ui/core/Fragment",
	"sap/ui/model/Sorter"

], function (Controller, Models, Filter, JSONModel, FilterOperator, Fragment, Sorter) {
	"use strict";

	return Controller.extend("training.training.controller.View1", {
		oTable: null,
		aVisibleColumns: [],
		_oDialog: null,
		_oPersonalizationDialog: null,
		_oDataInitial: null,
		onInit: function () {
			// Column configuration from models.js
			this.oConfigModel = Models.createColumnConfigModel();
			this.oConfigModel.setDefaultBindingMode("TwoWay");

			// Application table
			this.oTable = this.getView().byId("userTable");
			console.log("this.getVisibleColumns()", this.getVisibleColumns());
			this.oViewData = {
				activeFilters: [],
				activeSorters: [],
				visibleColumns: this.getVisibleColumns(),
				activeSearchFilters: []
			};
			this.oViewModel = new JSONModel(this.oViewData);
			this.oViewModel.setDefaultBindingMode("TwoWay");
			this.getView().setModel(this.oViewModel);

			// Load json model in table
			this.loadData();

		},
		/**
		 * Loads the data from the JSON file
		 * @public
		 */
		loadData: function () {
			var oModel = new JSONModel("./model/data.json");
			this.getView().setModel(oModel, "tableModel");
			oModel.setDefaultBindingMode("TwoWay");
		},

		getVisibleColumns: function () {
			var aColumns = this.oConfigModel.getData();
			return aColumns.filter(function (oColumn) {
				return oColumn.isVisible === true;
			});
		},

		onAfterRendering: function () {
			var aVisibleColumns = this.oViewData.visibleColumns;
			this._addColumn(this.oTable, aVisibleColumns);
		},

		onEnterSearch: function (oEvent) {
			var sQuery = oEvent.getParameters("query").query;
			this._onSearch(sQuery);
		},

		onLiveSearch: function (oEvent) {
			var sNewValue = oEvent.getParameters("newValue").newValue;
			this._onSearch(sNewValue);
		},

		onPersonalizationDialogPress: function (oEvent) {
			var that = this;
			var oView = this.getView()
			var fnOpen = function () {
				console.log("that.oViewData", that.oViewData)
				var aVisiblColumns = that.oViewData.visibleColumns;
				var aActiveFilters = that.oViewData.activeFilters;
				var aActiveSorters = that.oViewData.activeSorters;
				var aTempModel = new JSONModel({
					visibleColumns: aVisiblColumns.map(function (oItem) {
						return jQuery.extend(true, {}, oItem);
					}),
					activeFilters: aActiveFilters.map(function (oItem) {
						return jQuery.extend(true, {}, oItem);
					}),
					activeSorters: aActiveSorters.map(function (oItem) {
						return jQuery.extend(true, {}, oItem);
					}),
				});
				that._oPersonalizationDialog.setModel(aTempModel);
				that._oPersonalizationDialog.open();
			};
			// create dialog lazily
			if (!that._oPersonalizationDialog) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "training.training.fragment.Dialog",
					controller: this
				}).then(function (_oPersonalizationDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(_oPersonalizationDialog);
					that._oPersonalizationDialog = _oPersonalizationDialog;
					fnOpen();
				});
			} else {
				fnOpen();
			}
		},

		handleOK: function (oEvent) {
			this.getColumnCustomConfiguration();
			this.getSorterCustomConfiguration();
			this.getFilterCustomConfiguration();
			this._oPersonalizationDialog.close();
			this.oConfigModel.refresh();
		},

		getColumnCustomConfiguration: function () {
			var oPanelColumns = this._oPersonalizationDialog.getAggregation("panels")[0];
			var oVisibleColumns = oPanelColumns.getColumnsItems();
			var aColumns = oVisibleColumns.map(function (oVisibleColumn) {
				return {
					"id": oVisibleColumn.getProperty("columnKey"),
					"isVisible": oVisibleColumn.getProperty("visible"),
					"name": oVisibleColumn.getProperty("text")
				};
			});
			this.removeAllColumns();
			this._addColumn(this.oTable, aColumns);
			this.oViewData.visibleColumns = aColumns;
		},

		getSorterCustomConfiguration: function () {
			var oBinding = this.oTable.getBinding("rows");
			var oPanelSorting = this._oPersonalizationDialog.getAggregation("panels")[1];
			var aSorterConditions = oPanelSorting._getConditions();
			var aSorters = aSorterConditions.map(function (oSorterItem) {
				return new Sorter({
					path: oSorterItem.keyField,
					descending: oSorterItem.operation === "Ascending" ? false : true
				});
			});
			oBinding.sort(aSorters);
			this.oViewData.activeSorters = aSorterConditions;
		},

		getFilterCustomConfiguration: function () {
			var oBinding = this.oTable.getBinding("rows");
			var oPanelFilter = this._oPersonalizationDialog.getAggregation("panels")[2];
			var aConditions = oPanelFilter.getConditions();
			this.oViewModel.setProperty("/activeFilters", aConditions);
			var aFilters = aConditions.map(function (oFilterItem) {
				return new Filter({
					path: oFilterItem.keyField,
					operator: oFilterItem.operation,
					exclude: oFilterItem.exclude,
					value1: oFilterItem.value1
				});
			});
			oBinding.filter(new Filter({
				filters: aFilters,
				and: true
			}));
			this.oViewData.activeFilters = aConditions;
		},

		onReset: function () {
			// console.log("this._oPersonalizationDialog", this._oPersonalizationDialog.getAggregation("P13nFilterPanel"))
			var aConditions = this._oPersonalizationDialog.getAggregation("panels")[0].removeAllFilterItems();
			this._oPersonalizationDialog.close();
			console.log("aConditions", aConditions)
				// var oModel = new JSONModel("./model/data.json");
				// this.getView().setModel(oModel, "tableModel");

			// this.oJSONModel.setProperty("/", jQuery.extend(true, {}, this.oDataInitial));
		},
		onFilterItemRemove: function (oEvent) {},
		onFilterItemUpdate: function (oEvent) {},
		getFilterPanel: function () {
			return this.oPersonalizationDialog.getPanels()[0];
		},
		onCancel: function (oEvent) {
			console.log("cancel")
			this._oPersonalizationDialog.close();
		},

		_onSearch: function (sQuery) {
			var oSearchFilter = null;
			var oBinding = this.oTable.getBinding("rows");
			if (sQuery) {
				var aSearchFilters = [];
				var aStringFilters = this.aVisibleColumns;
				aSearchFilters = aStringFilters.map(function (aColumn) {
					return new Filter({
						path: aColumn.id,
						operator: FilterOperator.Contains,
						value1: sQuery
					});
				});
				oSearchFilter = new Filter({
					filters: aSearchFilters
				});

			}
			oBinding.filter(oSearchFilter);
		},

		removeAllColumns: function () {
			this.oTable.removeAllColumns();
		},

		/**
		 * Adds columns dynamically to the table
		 * @private
		 * @returns {aColumns}, which represents an array of the added Columns (those with the property of isVisible===true)
		 */
		_addColumn: function (oTable, aColumns) {
			// var aVisibleColumns = aColumns.filter(function (oColumn) {
			// 	return oColumn.isVisible;
			// });
			aColumns.forEach(function (col) {
				var oColumn = new sap.ui.table.Column({
					name: col.name,
					label: col.name,
					width: "207px",
					visible: col.isVisible,
					template: new sap.m.Text({
						text: {
							path: 'tableModel>' + col.id
						}
					})
				});
				oTable.addColumn(oColumn);
			});
		}
	});
});